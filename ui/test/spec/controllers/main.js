'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('angularUnitTestProjectApp'));

  var MainCtrl,$http,$httpBackend,scope,ubusService;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope,ubusService) {
    scope = $rootScope.$new();

    MainCtrl = $controller('MainCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  beforeEach(inject(function (_$http_, _$httpBackend_,_ubusService_) {
    $http=_$http_;
    $httpBackend=_$httpBackend_;
    ubusService = _ubusService_;

  }));

//  afterEach(function() {
//     $httpBackend.verifyNoOutstandingExpectation();
//     $httpBackend.verifyNoOutstandingRequest();
//   });

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });

  it('It should be firstName of list', function () {
     var response=[{"id": "1","name": "kalyan"}, {"id": "2","name": "dinakar"}];
    $httpBackend.whenGET(scope.url).respond(response);
	  ubusService.getList();
	  $httpBackend.flush();
    expect(scope.values[0]).toEqual('kalyan');
    expect(scope.values[0]).toEqual('sandeep');

  });

//  it('It should be firstName post of list', function () {
//    var response=[{
//		"id": "1",
//		"name": "kalyan"
//	}, {
//		"id": "2",
//		"name": "dinakar"
//	}];
//  $httpBackend.whenPOST(scope.url).respond(response);
//	  ubusService.getPostList();
//	  $httpBackend.flush();
//    expect(scope.values[0]).toEqual('kalyan');
//  });
});
