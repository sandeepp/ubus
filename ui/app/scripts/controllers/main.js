'use strict';

/**
 * @ngdoc function
 * @name angularUnitTestProjectApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularUnitTestProjectApp
 */
angular.module('angularUnitTestProjectApp')
  .provider('ubusService', function () {
    var baseUrl;
    var sessionId;
    return {
      setUrl: function (value) {
        baseUrl = value;
      },
      $get: function ($http, $q) {
        return {
        getSessionId: function (timeout) {
            var deferred = $q.defer();
            $http({
              method: 'POST',
              url: baseUrl+'session/create',
              data:{ "timeout": timeout }
            }).success(function (data) {
               //sessionId=data.sessionId
              deferred.resolve(data);
            }).error(function () {
              deferred.reject('There was an error');
            })

            return deferred.promise;
        },
        sessionList: function () {
            var deferred = $q.defer();
            $http({
              method: 'POST',
              url: baseUrl+'session/list',
              data:{ "ubus_rpc_session": sessionId}
            }).success(function (data) {
              deferred.resolve(data);
            }).error(function () {
              deferred.reject('There was an error');
            })

            return deferred.promise;
        },

        login: function (uname,pwd,timeout) {
            var deferred = $q.defer();
            $http({
              method: 'POST',
              url: baseUrl+'session/login',
              data:{ "username": uname,
                    "password": pwd,
                    "timeout": timeout }
            }).success(function (data) {
              deferred.resolve(data);
            }).error(function () {
              deferred.reject('There was an error');
            })

            return deferred.promise;
        },
        netWorkRestart: function () {
            var deferred = $q.defer();
            $http({
              method: 'POST',
              url: baseUrl+'network/restart',
              data:{}
            }).success(function (data) {
              deferred.resolve(data);
            }).error(function () {
              deferred.reject('There was an error');
            })

            return deferred.promise;
        },
        netWorkDevice: function () {
            var deferred = $q.defer();
            $http({
              method: 'POST',
              url: baseUrl+'network.device/status',
              data:{ "name": "ifname" }
            }).success(function (data) {
              deferred.resolve(data);
            }).error(function () {
              deferred.reject('There was an error');
            })

            return deferred.promise;
        },
        iwinfoDevice: function () {
            var deferred = $q.defer();
            $http({
              method: 'POST',
              url: baseUrl+'iwinfo/devices',
              data:{}
            }).success(function (data) {
              deferred.resolve(data);
            }).error(function () {
              deferred.reject('There was an error');
            })

            return deferred.promise;
        },
        iwinfoInfo: function () {
            var deferred = $q.defer();
            $http({
              method: 'POST',
              url: baseUrl+'iwinfo/info',
              data:{ "device": "device" }
            }).success(function (data) {
              deferred.resolve(data);
            }).error(function () {
              deferred.reject('There was an error');
            })

            return deferred.promise;
        },
        uciGet: function () {
            var deferred = $q.defer();
            $http({
              method: 'POST',
              url: baseUrl+'uci/get',
              data:{ "package": "package",
                    "section": "sname",
                    "type":    "type",
                    "option":  "oname" }
            }).success(function (data) {
              deferred.resolve(data);
            }).error(function () {
              deferred.reject('There was an error');
            })

            return deferred.promise;
        },
        uciAdd: function () {
            var deferred = $q.defer();
            $http({
              method: 'POST',
              url: baseUrl+'uci/add',
              data:{ "package": "package",
                    "type":    "type" }
            }).success(function (data) {
              deferred.resolve(data);
            }).error(function () {
              deferred.reject('There was an error');
            })

            return deferred.promise;
        },
        getList: function () {
            var deferred = $q.defer();
            $http({
              method: 'GET',
              url:'list.json'
            }).success(function (data) {
              deferred.resolve(data);
            }).error(function () {
              deferred.reject('There was an error');
            })

            return deferred.promise;
        },
        getPostList: function (name) {
            var deferred = $q.defer();
            $http({
              method: 'POST',
              url:'list.json',
              data:{"name":name}
            }).success(function (data) {
              deferred.resolve(data);
            }).error(function () {
              deferred.reject('There was an error');
            })

            return deferred.promise;
        }


      }
    }
  }
  })
  .config(function (ubusServiceProvider) {
      ubusServiceProvider.setUrl('http://your.server.ip/ubus/');
  })

  .controller('MainCtrl', function ($scope,$http,ubusService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.values=[];
    ubusService.getList()
    .then(function (data) {
       for(var i=0;i<data.length;i++){
          $scope.values.push(data[i].name);
        }
        console.log($scope.values)
    }, function (data) {
        alert(data);
    })

//ubusService.getSessionId(3000)
//      .then(function (data) {
//      console.log(data)
//      }, function (data) {
//        alert(data);
//      })



  });
