'use strict';

/**
 * @ngdoc function
 * @name angularUnitTestProjectApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularUnitTestProjectApp
 */
angular.module('angularUnitTestProjectApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
